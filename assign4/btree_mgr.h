#ifndef BTREE_MGR_H
#define BTREE_MGR_H

#include "dberror.h"
#include "tables.h"
#include "buffer_mgr.h"
#include "storage_mgr.h"

typedef struct BTreeHandle {
  DataType keyType;
  char *idxId;
  void *mgmtData;
} BTreeHandle;

typedef struct BT_ScanHandle {
  BTreeHandle *tree;
  void *mgmtData;
} BT_ScanHandle;


extern RC initIndexManager (void *mgmtData);
extern RC shutdownIndexManager ();

extern RC createBtree (char *idxId, DataType keyType, int n);
extern RC openBtree (BTreeHandle **tree, char *idxId);
extern RC closeBtree (BTreeHandle *tree);
extern RC deleteBtree (char *idxId);

extern RC getNumNodes (BTreeHandle *tree, int *result);
extern RC getNumEntries (BTreeHandle *tree, int *result);
extern RC getKeyType (BTreeHandle *tree, DataType *result);

extern RC findKey (BTreeHandle *tree, Value *key, RID *result);
extern RC insertKey (BTreeHandle *tree, Value *key, RID rid);
extern RC deleteKey (BTreeHandle *tree, Value *key);
extern RC openTreeScan (BTreeHandle *tree, BT_ScanHandle **handle);
extern RC nextEntry (BT_ScanHandle *handle, RID *result);
extern RC closeTreeScan (BT_ScanHandle *handle);

extern char *printTree (BTreeHandle *tree);


typedef struct BTNodeVal {
	RID rid;
} BTNodeVal;

typedef struct TNode {
	void ** ps;
	Value ** ks;
	struct TNode * father;
	bool is_leaf;
	int n_ks;
	struct TNode * next; 
} TNode;

typedef struct BT_TreeManager {
	BM_BufferPool bufferPool;
	BM_PageHandle pageHandler;
	int order;
	int nNodes;
	int nEntries;
	TNode * root;
	TNode * queue;
	DataType keyType;
} BT_TreeManager;

typedef struct ScanManager {
	int keyIdx;
	int cntKeys;
	int order;
	TNode * node;
} ScanManager;

BTNodeVal * findRd(TNode * root, Value * k);
void push(BT_TreeManager * tm, TNode * nd);
TNode * pop(BT_TreeManager * tm);
TNode * findLeaf(TNode * root, Value * k);
int rootWay(TNode * root, TNode * child);
bool LessThan(Value* k1, Value* k2);
bool GreatThan(Value* k1, Value* k2);
bool Equal(Value* k1, Value* k2);

TNode * changeRoot(TNode * root);
TNode * combineNodes(BT_TreeManager * tm, TNode * n, TNode * nb, int nb_idx, int k);
TNode * allocationNodes(TNode * root, TNode * n, TNode * nb, int nb_idx, int k_idx, int k);
TNode * removeEntry(BT_TreeManager * tm, TNode * n, Value * k, void * p);
TNode * removek(BT_TreeManager * tm, Value * k);
TNode * removeEntryFromNode(BT_TreeManager * tm, TNode * n, Value * k, TNode * p);
int getNbIdx(TNode * n);

BTNodeVal * makeRecord(RID * rid);
TNode * addLeaf(BT_TreeManager * tm, TNode * leaf, Value * k, BTNodeVal * p);
TNode * createNewTree(BT_TreeManager * tm, Value * key, BTNodeVal * p);
TNode * createNode(BT_TreeManager * tm);
TNode * createLeaf(BT_TreeManager * tm);
TNode * addLeafSplit(BT_TreeManager * tm, TNode * leaf, Value * k, BTNodeVal * p);
TNode * addNode(BT_TreeManager * tm, TNode * parent, int l_idx, Value * k, TNode * r);
TNode * addNodeSplit(BT_TreeManager * tm, TNode * parent, int l_idx, Value * k, TNode * r);
TNode * addFather(BT_TreeManager * tm, TNode * l, Value * k, TNode * r);
TNode * addNextRoot(BT_TreeManager * tm, TNode * l, Value * k, TNode * r);
int getLeftIdx(TNode * parent, TNode * l);





#endif // BTREE_MGR_H
