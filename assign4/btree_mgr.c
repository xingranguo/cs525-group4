#include "btree_mgr.h"
#include <stdlib.h>
#include <string.h>

BT_TreeManager * btreemgr = NULL;
RC initIndexManager(void *mgmtData)
{
	printf("Welcome to initIndexManager 2021-12-7.\n");
    return RC_OK;
}

RC shutdownIndexManager()
{
    return RC_OK;
}

RC openBtree(BTreeHandle **tree, char *idxId) 
{
	*tree = (BTreeHandle*)malloc(sizeof(BTreeHandle));
	(*tree)->mgmtData = btreemgr;

	return initBufferPool(&btreemgr->bufferPool, idxId, 1000, RS_FIFO, NULL);
}

RC createBtree(char *idxId, DataType keyType, int n) 
{
	if(n > PAGE_SIZE/sizeof(TNode)) 
		return -1;

	btreemgr = (BT_TreeManager *) malloc(sizeof(BT_TreeManager));
	btreemgr->order = n + 2;
	btreemgr->nNodes = 0;
	btreemgr->nEntries = 0;
	btreemgr->root = NULL;
	btreemgr->queue = NULL;
	btreemgr->keyType = keyType;

	BM_BufferPool * bm = (BM_BufferPool *) malloc(sizeof(BM_BufferPool));
	btreemgr->bufferPool = *bm;
	SM_FileHandle fh;
	char data[PAGE_SIZE];

	if(0 != createPageFile(idxId))
		return -1;
	if(0 != openPageFile(idxId, &fh))
		return -2;
	if(0 != writeBlock(0, &fh, data))
		return -3;
	if(0 != closePageFile(&fh))
		return -4;

	return RC_OK;
}

RC closeBtree(BTreeHandle *tree) 
{
	BT_TreeManager * btreemgr = (BT_TreeManager*) tree->mgmtData;
	markDirty(&btreemgr->bufferPool, &btreemgr->pageHandler);
	shutdownBufferPool(&btreemgr->bufferPool);
	free(btreemgr);
	free(tree);
	return RC_OK;
}

RC deleteBtree(char *idxId) 
{
	return destroyPageFile(idxId);
}

RC getNumNodes(BTreeHandle *tree, int *result) 
{
	BT_TreeManager* btreemgr = (BT_TreeManager*)tree->mgmtData;
	*result = btreemgr->nNodes;
	return RC_OK;
}

RC findKey(BTreeHandle *tree, Value *key, RID *result) 
{
	BT_TreeManager *btreemgr = (BT_TreeManager *) tree->mgmtData;
	BTNodeVal * r = findRd(btreemgr->root, key);

	if(r == NULL)
		return RC_IM_KEY_NOT_FOUND;
	*result = r->rid;
	return RC_OK;
}

RC getNumEntries(BTreeHandle *tree, int *result) 
{
	BT_TreeManager* btreemgr = (BT_TreeManager*) tree->mgmtData;
	*result = btreemgr->nEntries;
	return RC_OK;
}

RC getKeyType(BTreeHandle *tree, DataType *result) 
{
	BT_TreeManager* btreemgr = (BT_TreeManager*)tree->mgmtData;
	*result = btreemgr->keyType;
	return RC_OK;
}

RC deleteKey(BTreeHandle *tree, Value *key) 
{
	BT_TreeManager *btreemgr = (BT_TreeManager *) tree->mgmtData;
	btreemgr->root = removek(btreemgr, key);
	return RC_OK;
}

RC insertKey(BTreeHandle *tree, Value *key, RID rid) 
{
	BT_TreeManager *btreemgr = (BT_TreeManager *) tree->mgmtData;
	BTNodeVal * pointer;
	TNode * leaf;

	int BTOrder = btreemgr->order;
	if (findRd(btreemgr->root, key) != NULL) 
		return -1;
	pointer = makeRecord(&rid);

	if (btreemgr->root == NULL) 
	{
		btreemgr->root = createNewTree(btreemgr, key, pointer);
		return RC_OK;
	}

	leaf = findLeaf(btreemgr->root, key);
	if(leaf->n_ks < BTOrder - 1)
		leaf = addLeaf(btreemgr, leaf, key, pointer);
	else 
		btreemgr->root = addLeafSplit(btreemgr, leaf, key, pointer);

	return RC_OK;
}

RC openTreeScan(BTreeHandle *tree, BT_ScanHandle **handle) 
{
	BT_TreeManager *btreemgr = (BT_TreeManager *) tree->mgmtData;
	ScanManager *sm = malloc(sizeof(ScanManager));

	*handle = malloc(sizeof(BT_ScanHandle));
	TNode * node = btreemgr->root;

	if (btreemgr->root == NULL) 
	{
		return -1;
	} else 
	{
		while (!node->is_leaf)
			node = node->ps[0];

		sm->keyIdx = 0;
		sm->cntKeys = node->n_ks;
		sm->node = node;
		sm->order = btreemgr->order;
		(*handle)->mgmtData = sm;
	}
	return RC_OK;
}

RC nextEntry(BT_ScanHandle *handle, RID *result) 
{
	ScanManager * sm = (ScanManager *) handle->mgmtData;
	int keyIdx = sm->keyIdx;
	int cntKeys = sm->cntKeys;
	int btOrder = sm->order;
	RID rid;

	TNode * node = sm->node;

	if(node == NULL) 
		return RC_IM_NO_MORE_ENTRIES;

	if(keyIdx < cntKeys) 
	{
		rid = ((BTNodeVal *) node->ps[keyIdx])->rid;
		sm->keyIdx++;
	} 
	else 
	{
		if(node->ps[btOrder - 1] != NULL) 
		{
			node = node->ps[btOrder - 1];
			sm->keyIdx = 1;
			sm->cntKeys = node->n_ks;
			sm->node = node;
			rid = ((BTNodeVal *) node->ps[0])->rid;
		}
		else 
		{
			return RC_IM_NO_MORE_ENTRIES;
		}
	}
	*result = rid;
	return RC_OK;
}

RC closeTreeScan(BT_ScanHandle *handle) 
{
	handle->mgmtData = NULL;
	free(handle);
	return RC_OK;
}

BTNodeVal* makeRecord(RID* rid) 
{
	BTNodeVal* record = (BTNodeVal*)malloc(sizeof(BTNodeVal));
	if(record == NULL) 
	{
		exit(-100);
	} 
	else 
	{
		record->rid.page = rid->page;
		record->rid.slot = rid->slot;
	}
	return record;
}

TNode * createNewTree(BT_TreeManager* btreemgr, Value* key, BTNodeVal* pointer) 
{
	TNode* root = createLeaf(btreemgr);
	int btOrder = btreemgr->order;

	root->ks[0] = key;
	root->ps[0] = pointer;
	root->ps[btOrder - 1] = NULL;
	root->father = NULL;
	root->n_ks++;

	btreemgr->nEntries++;
	return root;
}

bool LessThan(Value* k1, Value* k2) 
{
	if(k1->dt == DT_INT)
	{
		if(k1->v.intV < k2->v.intV)
			return true;
		else
			return false;
	}
}

bool GreatThan(Value * k1, Value * k2) 
{
	if(k1->dt == DT_INT)
	{
		if(k1->v.intV > k2->v.intV)
			return true;
		else
			return false;
	}
}

bool Equal(Value* k1, Value* k2) 
{
	if(k1->dt == DT_INT)
	{
		if(k1->v.intV == k2->v.intV)
			return true;
		else
			return false;
	}
}

TNode * addLeaf(BT_TreeManager * btreemgr, TNode * leaf, Value * key, BTNodeVal * pointer) 
{
	int i, add_p;
	btreemgr->nEntries++;

	add_p = 0;
	while(add_p < leaf->n_ks && LessThan(leaf->ks[add_p], key))
		add_p++;

	for(i = leaf->n_ks; i > add_p; i--) 
	{
		leaf->ks[i] = leaf->ks[i - 1];
		leaf->ps[i] = leaf->ps[i - 1];
	}
	leaf->ks[add_p] = key;
	leaf->ps[add_p] = pointer;
	leaf->n_ks++;
	return leaf;
}

TNode* addLeafSplit(BT_TreeManager* btreemgr, TNode* leaf, Value* key, BTNodeVal* pointer) 
{
	TNode* new_leaf;
	Value** t_keys;
	void** t_pointers;
	int add_idx, split, new_key, i, j;

	new_leaf = createLeaf(btreemgr);
	int BTOrder = btreemgr->order;

	t_keys = malloc(BTOrder*sizeof(Value));
	if(t_keys == NULL) 
		exit(-100);

	t_pointers = malloc(BTOrder*sizeof(void*));
	if(t_pointers == NULL)
		exit(-1);

	add_idx = 0;
	while(add_idx < BTOrder - 1 && LessThan(leaf->ks[add_idx], key))
		add_idx++;

	for (i = 0, j = 0; i < leaf->n_ks; i++, j++) {
		if (j == add_idx)
			j++;
		t_keys[j] = leaf->ks[i];
		t_pointers[j] = leaf->ps[i];
	}

	t_keys[add_idx] = key;
	t_pointers[add_idx] = pointer;

	leaf->n_ks = 0;

	// Splitting
	if ((BTOrder - 1) % 2 == 0)
		split = (BTOrder - 1) / 2;
	else
		split = (BTOrder - 1) / 2 + 1;

	for (i = 0; i < split; i++) {
		leaf->ps[i] = t_pointers[i];
		leaf->ks[i] = t_keys[i];
		leaf->n_ks++;
	}

	for(i = split, j = 0; i < BTOrder; i++, j++) 
	{
		new_leaf->ps[j] = t_pointers[i];
		new_leaf->ks[j] = t_keys[i];
		new_leaf->n_ks++;
	}

	free(t_pointers);
	free(t_keys);

	new_leaf->ps[BTOrder - 1] = leaf->ps[BTOrder - 1];
	leaf->ps[BTOrder - 1] = new_leaf;

	for(i = leaf->n_ks; i < BTOrder - 1; i++)
		leaf->ps[i] = NULL;
	for(i = new_leaf->n_ks; i < BTOrder - 1; i++)
		new_leaf->ps[i] = NULL;

	new_leaf->father = leaf->father;
	new_key = new_leaf->ks[0];
	btreemgr->nEntries++;
	return addFather(btreemgr, leaf, new_key, new_leaf);
}


TNode* addNodeSplit(BT_TreeManager* btreemgr, TNode* old_node, int left_idx, Value* key, TNode* right) 
{
	int i, j, split, k_p;
	TNode * nd, *child;
	Value ** t_keys;
	TNode ** t_pointers;

	int BTOrder = btreemgr->order;

	t_pointers = malloc((BTOrder + 1)*sizeof(TNode*));
	if(t_pointers == NULL)
		exit(-100);

	t_keys = malloc(BTOrder*sizeof(Value*));
	if(t_keys == NULL)
		exit(-100);

	for(i = 0, j = 0; i < old_node->n_ks + 1; i++, j++) 
	{
		if(j == left_idx + 1)
			j++;
		t_pointers[j] = old_node->ps[i];
	}

	for(i = 0, j = 0; i < old_node->n_ks; i++, j++) 
	{
		if(j == left_idx)
			j++;
		t_keys[j] = old_node->ks[i];
	}

	t_pointers[left_idx + 1] = right;
	t_keys[left_idx] = key;

	if((BTOrder - 1) % 2 == 0)
		split = (BTOrder - 1) / 2;
	else
		split = (BTOrder - 1) / 2 + 1;

	nd = createNode(btreemgr);
	old_node->n_ks = 0;
	for(i = 0; i < split - 1; i++) 
	{
		old_node->ps[i] = t_pointers[i];
		old_node->ks[i] = t_keys[i];
		old_node->n_ks++;
	}
	old_node->ps[i] = t_pointers[i];
	k_p = t_keys[split - 1];
	for(++i, j = 0; i < BTOrder; i++, j++) 
	{
		nd->ps[j] = t_pointers[i];
		nd->ks[j] = t_keys[i];
		nd->n_ks++;
	}
	nd->ps[j] = t_pointers[i];
	free(t_pointers);
	free(t_keys);
	nd->father = old_node->father;
	for(i = 0; i <= nd->n_ks; i++) 
	{
		child = nd->ps[i];
		child->father = nd;
	}

	btreemgr->nEntries++;
	return addFather(btreemgr, old_node, k_p, nd);
}

TNode* addFather(BT_TreeManager* btreemgr, TNode* left, Value* key, TNode* right) 
{
	int left_idx;
	TNode * father = left->father;
	int BTOrder = btreemgr->order;

	if(father == NULL)
		return addNextRoot(btreemgr, left, key, right);

	left_idx = getLeftIdx(father, left);

	if(father->n_ks < BTOrder - 1)
		return addNode(btreemgr, father, left_idx, key, right);

	return addNodeSplit(btreemgr, father, left_idx, key, right);
}

int getLeftIdx(TNode* father, TNode* left) 
{
	int left_idx = 0;
	while(left_idx <= father->n_ks && father->ps[left_idx] != left)
		left_idx++;
	return left_idx;
}

TNode* addNode(BT_TreeManager* btreemgr, TNode* father, int left_idx, Value* key, TNode* right) 
{
	int i;
	for(i = father->n_ks; i > left_idx; i--) 
	{
		father->ps[i + 1] = father->ps[i];
		father->ks[i] = father->ks[i - 1];
	}

	father->ps[left_idx + 1] = right;
	father->ks[left_idx] = key;
	father->n_ks++;

	return btreemgr->root;
}


TNode* addNextRoot(BT_TreeManager* btreemgr, TNode* left, Value* key, TNode* right) 
{
	TNode* root = createNode(btreemgr);
	root->ks[0] = key;
	root->ps[0] = left;
	root->ps[1] = right;
	root->n_ks++;
	root->father = NULL;
	left->father = root;
	right->father = root;
	return root;
}

TNode* createNode(BT_TreeManager* btreemgr) 
{
	btreemgr->nNodes++;
	int BTOrder = btreemgr->order;

	TNode* nd = malloc(sizeof(TNode));
	if(nd == NULL)
		exit(-1);

	nd->ks = malloc((BTOrder - 1)*sizeof(Value*));
	if(nd->ks == NULL)
		exit(-1);

	nd->ps = malloc(BTOrder*sizeof(void*));
	if (nd->ps == NULL)
		exit(-1);

	nd->is_leaf = false;
	nd->n_ks = 0;
	nd->father = NULL;
	nd->next = NULL;
	return nd;
}

TNode* createLeaf(BT_TreeManager* btreemgr) 
{
	TNode* leaf = createNode(btreemgr);
	leaf->is_leaf = true;
	return leaf;
}

TNode* findLeaf(TNode* root, Value* key) 
{
	int i = 0;
	TNode* c = root;
	if (c == NULL)
		return c;

	while(!c->is_leaf) 
	{
		i = 0;
		while (i < c->n_ks) 
		{
			if (GreatThan(key, c->ks[i]) || Equal(key, c->ks[i])) 
			{
				i++;
			} 
			else
				break;
		}
		c = (TNode*)c->ps[i];
	}
	return c;
}

BTNodeVal* findRd(TNode* root, Value* key) 
{
	int i = 0;
	TNode* c = findLeaf(root, key);
	if (c == NULL)
		return NULL;
	for(i = 0; i < c->n_ks; i++) 
	{
		if(Equal(c->ks[i], key))
			break;
	}
	if(i == c->n_ks)
		return NULL;
	else
		return (BTNodeVal*)c->ps[i];
}


int getNbIdx(TNode* n) 
{

	int i;
	for(i = 0; i <= n->father->n_ks; i++)
		if(n->father->ps[i] == n)
			return i - 1;

	exit(-1);
}

TNode* removeEntryFromNode(BT_TreeManager* btreemgr, TNode* n, Value* key, TNode* pointer) 
{
	int i, num_pointers;
	int BTOrder = btreemgr->order;

	i = 0;
	while(!Equal(n->ks[i], key))
		i++;

	for(++i; i < n->n_ks; i++)
		n->ks[i - 1] = n->ks[i];

	num_pointers = n->is_leaf ? n->n_ks : n->n_ks + 1;
	i = 0;
	while(n->ps[i] != pointer)
		i++;
	for(++i; i < num_pointers; i++)
		n->ps[i - 1] = n->ps[i];

	n->n_ks--;
	btreemgr->nEntries--;

	if(n->is_leaf)
		for(i = n->n_ks; i < BTOrder - 1; i++)
			n->ps[i] = NULL;
	else
		for(i = n->n_ks + 1; i < BTOrder; i++)
			n->ps[i] = NULL;

	return n;
}

TNode* changeRoot(TNode* root) 
{
	TNode* new_root;

	if(root->n_ks > 0)
		return root;

	if(!root->is_leaf) 
	{
		new_root = root->ps[0];
		new_root->father = NULL;
	} 
	else 
	{
		new_root = NULL;
	}

	free(root->ks);
	free(root->ps);
	free(root);
	return new_root;
}

TNode* combineNodes(BT_TreeManager* btreemgr, TNode* n, TNode* nb, int nb_idx, int k_p) 
{
	int i, j, nb_add_idx, n_end;
	TNode* tmp;
	int BTOrder = btreemgr->order;

	if(nb_idx == -1) 
	{
		tmp = n;
		n = nb;
		nb = tmp;
	}

	nb_add_idx = nb->n_ks;

	if(!n->is_leaf) 
	{
		nb->ks[nb_add_idx] = k_p;
		nb->n_ks++;

		n_end = n->n_ks;

		for(i = nb_add_idx + 1, j = 0; j < n_end; i++, j++) 
		{
			nb->ks[i] = n->ks[j];
			nb->ps[i] = n->ps[j];
			nb->n_ks++;
			n->n_ks--;
		}

		nb->ps[i] = n->ps[j];

		for(i = 0; i < nb->n_ks + 1; i++) 
		{
			tmp = (TNode *) nb->ps[i];
			tmp->father = nb;
		}
	} 
	else 
	{
		for(i = nb_add_idx, j = 0; j < n->n_ks; i++, j++) 
		{
			nb->ks[i] = n->ks[j];
			nb->ps[i] = n->ps[j];
			nb->n_ks++;
		}
		nb->ps[BTOrder - 1] = n->ps[BTOrder - 1];
	}

	btreemgr->root = removeEntry(btreemgr, n->father, k_p, n);

	free(n->ks);
	free(n->ps);
	free(n);
	return btreemgr->root;
}

TNode* removeEntry(BT_TreeManager* btreemgr, TNode* n, Value* key, void* pointer) 
{
	int min_keys;
	TNode* nb;
	int nb_idx;
	int k_p_idx, k_p;
	int capacity;
	int BTOrder = btreemgr->order;

	n = removeEntryFromNode(btreemgr, n, key, pointer);

	if (n == btreemgr->root)
		return changeRoot(btreemgr->root);

	if(n->is_leaf) 
	{
		if((BTOrder - 1) % 2 == 0)
			min_keys = (BTOrder - 1) / 2;
		else
			min_keys = (BTOrder - 1) / 2 + 1;
	} 
	else 
	{
		if((BTOrder) % 2 == 0)
			min_keys = (BTOrder) / 2;
		else
			min_keys = (BTOrder) / 2 + 1;
		min_keys--;
	}

	if(n->n_ks >= min_keys)
		return btreemgr->root;

	nb_idx = getNbIdx(n);
	k_p_idx = nb_idx == -1 ? 0 : nb_idx;
	k_p = n->father->ks[k_p_idx];
	nb =
			(nb_idx == -1) ? n->father->ps[1] : n->father->ps[nb_idx];

	capacity = n->is_leaf ? BTOrder : BTOrder - 1;

	if(nb->n_ks + n->n_ks < capacity)
		return combineNodes(btreemgr, n, nb, nb_idx, k_p);
	else
		return allocationNodes(btreemgr->root, n, nb, nb_idx, k_p_idx, k_p);
}

TNode* removek(BT_TreeManager* btreemgr, Value* key) 
{
	TNode* record = findRd(btreemgr->root, key);
	BTNodeVal* key_leaf = findLeaf(btreemgr->root, key);

	if(record != NULL && key_leaf != NULL) 
	{
		btreemgr->root = removeEntry(btreemgr, key_leaf, key, record);
		free(record);
	}
	return btreemgr->root;
}

TNode* allocationNodes(TNode* root, TNode* n, TNode* nb, int nb_idx, int k_p_idx, int k_p) 
{
	int i;
	TNode* tmp;

	if(nb_idx != -1) 
	{
		if(!n->is_leaf)
			n->ps[n->n_ks + 1] = n->ps[n->n_ks];
		for(i = n->n_ks; i > 0; i--) 
		{
			n->ks[i] = n->ks[i - 1];
			n->ps[i] = n->ps[i - 1];
		}
		if(!n->is_leaf) 
		{
			n->ps[0] = nb->ps[nb->n_ks];
			tmp = (TNode *) n->ps[0];
			tmp->father = n;
			nb->ps[nb->n_ks] = NULL;
			n->ks[0] = k_p;
			n->father->ks[k_p_idx] = nb->ks[nb->n_ks - 1];
		} 
		else 
		{
			n->ps[0] = nb->ps[nb->n_ks - 1];
			nb->ps[nb->n_ks - 1] = NULL;
			n->ks[0] = nb->ks[nb->n_ks - 1];
			n->father->ks[k_p_idx] = n->ks[0];
		}
	} 
	else 
	{
		if(n->is_leaf) 
		{
			n->ks[n->n_ks] = nb->ks[0];
			n->ps[n->n_ks] = nb->ps[0];
			n->father->ks[k_p_idx] = nb->ks[1];
		} 
		else 
		{
			n->ks[n->n_ks] = k_p;
			n->ps[n->n_ks + 1] = nb->ps[0];
			tmp = (TNode *) n->ps[n->n_ks + 1];
			tmp->father = n;
			n->father->ks[k_p_idx] = nb->ks[0];
		}
		for(i = 0; i < nb->n_ks - 1; i++) 
		{
			nb->ks[i] = nb->ks[i + 1];
			nb->ps[i] = nb->ps[i + 1];
		}
		if(!n->is_leaf)
			nb->ps[i] = nb->ps[i + 1];
	}

	n->n_ks++;
	nb->n_ks--;

	return root;
}

void push(BT_TreeManager* btreemgr, TNode* nd) 
{
	TNode* c;
	if (btreemgr->queue == NULL) 
	{
		btreemgr->queue = nd;
		btreemgr->queue->next = NULL;
	} 
	else 
	{
		c = btreemgr->queue;
		while (c->next != NULL) 
		{
			c = c->next;
		}
		c->next = nd;
		nd->next = NULL;
	}
}

TNode* pop(BT_TreeManager* btreemgr)
{
	TNode* t = btreemgr->queue;
	btreemgr->queue = btreemgr->queue->next;
	t->next = NULL;
	return t;
}

int rootway(TNode* root, TNode* child) 
{
	int size = 0;
	TNode* t = child;
	while(t != root) 
	{
		t = t->father;
		++size;
	}
	return size;
}
