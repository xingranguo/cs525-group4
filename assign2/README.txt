===0. project dir tree
assign2
--- README.txt
--- Makefile
--- buffer_mgr.h
--- buffer_mgr.c
--- buffer_mgr_stat.h
--- buffer_mgr_stat.c
--- dberror.h
--- dberror.c
--- dt.h
--- storage_mgr.h
--- storage_mgr.c
--- test_assign2_1.c
--- test_assign2_2.c
--- test_helper.h

Cooperated with: 
Lei Chen    A20490798 isolatedream@gmail.com
Xingran GUO A20479094 1516539616@qq.com
Lang PAN    A20474847 panlang@mail.ustc.edu.cn


===1. compiler & run test_case
make
./test_assign2_1

===2. memory check 
valgrind --leak-check=full --show-reachable=yes --trace-children=yes ./test_assign2_1

===3. describes solution

// Define BM_PageFrame, store the information of the page frame.
typedef struct BM_PageFrame {
	char data[PAGE_SIZE];
    int pagenum;
    int fix;
    bool dirty;
    long long time_usec;
	struct BM_PageFrame* next;
} BM_PageFrame;

// Define BM_PageList, Link the page frame.
typedef struct BM_PageList {
    BM_PageFrame* head_;
    BM_PageFrame* latest_;
    BM_PageFrame* tail_;
    int pagesize_;
    int num_read_io_;
    int num_write_io_;
}BM_PageList;


// stores information about a buffer pool: 
// the name of the page file associated with the buffer pool (pageFile), 
// the number of page frames (numPages), 
// the page replacement strategy (strategy), 
// and a pointer to bookkeeping data(mgmtData).
// The member mgmtData of BM_BufferPool, points to the linked list(BM_PageList)
typedef struct BM_BufferPool {
	char *pageFile;
	int numPages;
	ReplacementStrategy strategy;
	void *mgmtData; // use this one to store the bookkeeping info your buffer
	// manager needs for a buffer pool
} BM_BufferPool;



// Buffer Manager Interface Pool Handling

// Initialize the bufferpool.
// Initially loaded file name, replacement algorithm, number of memory pages
RC initBufferPool(BM_BufferPool *const bm, const char *const pageFileName, 
		const int numPages, ReplacementStrategy strategy,
		void *stratData);

// 1.Write all dirty pages back to disk
// 2.Free page linked list memory
// 3.Release the buffer pool memory
RC shutdownBufferPool(BM_BufferPool *const bm);

// causes all dirty pages (with fix count 0) from the buffer pool to be written to disk.
RC forceFlushPool(BM_BufferPool *const bm);



// Buffer Manager Interface Access Pages

// marks a page as dirty.  dirty = true
RC markDirty (BM_BufferPool *const bm, BM_PageHandle *const page);

// unpins the page page, fix--
RC unpinPage (BM_BufferPool *const bm, BM_PageHandle *const page);

// write the current content of the page back to the page file on disk.
RC forcePage (BM_BufferPool *const bm, BM_PageHandle *const page);

// 1.If this page is in the buffer pool, return directly.
// 2. If this page is not in the buffer pool:  
// 	fifo:  
//	  If the buffer pool is not full, allocate a node directly at the end of the linked list, and load the disk data to the memory
//	  If the buffer pool is full, start from the next node of the last inserted node, find the first unpinPage replacement
// 	lru:
//    If the buffer pool is not full, allocate a node directly at the end of the linked list, and load the disk data to the memory
//    If the buffer pool is full, Find the memory page that has not been used for the longest time and replace it
RC pinPage (BM_BufferPool *const bm, BM_PageHandle *const page, 
		const PageNumber pageNum);



// Statistics Interface

// function returns an array of PageNumbers (of size numPages) where the
// ith element is the number of the page stored in the ith page frame. An empty page frame is
// represented using the constant NO PAGE.
PageNumber *getFrameContents (BM_BufferPool *const bm);

// function returns an array of bools (of size numPages) where the ith element
// is TRUE if the page stored in the ith page frame is dirty. Empty page frames are considered as clean.
bool *getDirtyFlags (BM_BufferPool *const bm);

// function returns an array of ints (of size numPages) where the ith element is
// the fix count of the page stored in the ith page frame. Return 0 for empty page frames
int *getFixCounts (BM_BufferPool *const bm);

// function returns the number of pages that have been read from disk since a
// buffer pool has been initialized.
int getNumReadIO (BM_BufferPool *const bm);

// returns the number of pages written to the page file since the buffer pool has been initialized.
int getNumWriteIO (BM_BufferPool *const bm);

// other Auxiliary function
// RS_LRU
RC lru(BM_BufferPool *const bm, BM_PageHandle *const page, 
		const PageNumber pageNum);

// RS_FIFO
RC fifo(BM_BufferPool *const bm, BM_PageHandle *const page, 
		const PageNumber pageNum);

// Get milliseconds
long long *getTimeUsecs (BM_BufferPool *const bm);
