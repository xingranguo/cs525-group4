#include "buffer_mgr.h"
#include <string.h>
#include "storage_mgr.h"
#include <assert.h>
#include <stdlib.h>
#include <sys/time.h>


typedef struct BM_PageFrame {
	char data[PAGE_SIZE];
    int pagenum;
    int fix;
    bool dirty;
    long long time_usec;
	struct BM_PageFrame* next;
} BM_PageFrame;

typedef struct BM_PageList {
    BM_PageFrame* head_;
    BM_PageFrame* latest_;
    BM_PageFrame* tail_;
    int pagesize_;
    int num_read_io_;
    int num_write_io_;
}BM_PageList;

RC initBufferPool(BM_BufferPool *const bm, const char *const pageFileName, 
		const int numPages, ReplacementStrategy strategy,
		void *stratData)
{
    bm->pageFile = (char*)pageFileName;
    bm->numPages = numPages;
    bm->strategy = strategy;
    bm->mgmtData = stratData;


    return RC_OK;
}

RC shutdownBufferPool(BM_BufferPool *const bm)
{
    if(bm == NULL)
        return RC_FILE_HANDLE_NOT_INIT;
    
    forceFlushPool(bm);

    // empty the linked list
    BM_PageList *list = bm->mgmtData;
    if(list == NULL)
        return RC_FILE_HANDLE_NOT_INIT;
    
    BM_PageFrame *p = list->head_;
    while(p != NULL)
    {
        BM_PageFrame *delete_node = p;
        p = p->next;
        free(delete_node);
    }
    list->head_ = NULL;

    if(bm->mgmtData != NULL)    
    {
        free(bm->mgmtData);
        bm->mgmtData = NULL;
    }

    return RC_OK;
}


RC forceFlushPool(BM_BufferPool *const bm)
{
    BM_PageList *list = bm->mgmtData;
    BM_PageFrame *p = list->head_;
    SM_FileHandle file_handle;
    int ret = openPageFile(bm->pageFile, &file_handle);
    if (RC_OK != ret)
        return ret;
    while(p != NULL)
    {
        if(/*p->fix == 0 &&*/ p->dirty == true)
        {
            // Refresh the corresponding page of the disk
            writeBlock(p->pagenum, &file_handle, p->data);
            ((BM_PageList*)(bm->mgmtData))->num_write_io_++;
            p->dirty = false;
        }
        p->fix = 0;
        p = p->next;
    }
    closePageFile(&file_handle);

    return RC_OK;
}

RC markDirty (BM_BufferPool *const bm, BM_PageHandle *const page)
{
    if(bm == NULL || page == NULL)
        return RC_FILE_HANDLE_NOT_INIT;
    BM_PageList *list = bm->mgmtData;
    BM_PageFrame *p = list->head_;
    while(p != NULL)
    {
        if(p->pagenum == page->pageNum)
        {
            p->dirty = true;
            return RC_OK;
        }
        p = p->next;
    }

    return RC_OK;
}

RC unpinPage (BM_BufferPool *const bm, BM_PageHandle *const page)
{
    if(bm == NULL || page == NULL)
        return RC_FILE_HANDLE_NOT_INIT;
    
    BM_PageList * list = bm->mgmtData;
    BM_PageFrame *p = list->head_;
    while(p != NULL)
    {
        if(p->pagenum == page->pageNum)
        {
            p->fix--;
            // memcpy(p->data, page->data, PAGE_SIZE);
            return RC_OK;
        }
        p = p->next;
    }

    return RC_OK;
}

// Write back dirty pages to disk
RC forcePage (BM_BufferPool *const bm, BM_PageHandle *const page)
{
    SM_FileHandle file_handle;
    int ret = openPageFile(bm->pageFile, &file_handle);
    if (RC_OK != ret)
        return ret;
    if(page != NULL)
    {
        writeBlock(page->pageNum, &file_handle, page->data);
        ((BM_PageList*)(bm->mgmtData))->num_write_io_++;
    }
    closePageFile(&file_handle);
    return RC_OK;
}

RC pinPage(BM_BufferPool *const bm, BM_PageHandle *const page, 
		const PageNumber pageNum)
{
    if(bm->strategy == RS_FIFO)
    {
        return fifo(bm, page, pageNum);
    }else if (bm->strategy == RS_LRU)
    {
        return lru(bm, page, pageNum);
    }else
    {
        return RC_RM_UNKOWN_DATATYPE;
    }
    
}

long long getUsecTime()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    // printf("%llu \n", tv.tv_sec*1000 + tv.tv_usec);
    return tv.tv_sec*1000 + tv.tv_usec;
}

RC lru(BM_BufferPool *const bm, BM_PageHandle *const page, 
		const PageNumber pageNum)
{
    // 1. Find the corresponding page in the bufferManager
    BM_PageList* list = NULL;
    list = bm->mgmtData;
    if(bm->mgmtData == NULL)
    {
        bm->mgmtData = malloc(sizeof(BM_PageList));
        list = bm->mgmtData;
        list->head_ = NULL;
        list->latest_ = NULL;
        list->pagesize_ = 0;
        list->tail_ = NULL;
        list->num_read_io_ = 0;
        list->num_write_io_ = 0;

    }

    BM_PageFrame *p = list->head_;
    while(p != NULL)
    {
        if(p->pagenum == pageNum)
        {
            page->pageNum = p->pagenum;
            p->fix++;
            // memcpy(page->data, p->data, PAGE_SIZE);
            p->time_usec = getUsecTime();
            return RC_OK;
        }
        p = p->next;
    }

    // 2.1 If the memory does not find the corresponding page, load it from the disk manager to the memory manager
    SM_FileHandle file_handle;
    int ret = openPageFile(bm->pageFile, &file_handle);
    if (RC_OK != ret)
        return ret;
    
    
    // 2.2 If there are not enough pages on the disk, create an empty page to fill
    if(file_handle.totalNumPages == pageNum)
    {
        SM_PageHandle mem = (char*)malloc(PAGE_SIZE);
        memset(mem, '\0', PAGE_SIZE);
        writeBlock(pageNum, &file_handle, mem);
        free(mem);
    }

    // 2.3 Through the disk page handle, take out the corresponding page
    SM_PageHandle page_handle = (char*)malloc(PAGE_SIZE);
    ret = readBlock(pageNum, &file_handle, page_handle);
    ((BM_PageList*)(bm->mgmtData))->num_read_io_++;
    if(RC_OK != ret)
        return ret;
    closePageFile(&file_handle); 
    // 3.1 If the memory page linked list is empty, then link to the head
    // p = bm->mgmtData;
    p = list->head_;
    if(p == NULL) // pool is empty.
    {
        // printf("pool is empty. \n");
        p = (void *)malloc(sizeof(BM_PageFrame));
        // p = bm->mgmtData;
        p->next = NULL;
        p->pagenum = pageNum;
        memcpy( p->data, page_handle, PAGE_SIZE);
        p->fix = 0;
        p->fix++;
        p->dirty = false;
        page->data = p->data;
        page->pageNum = p->pagenum;
        p->time_usec = getUsecTime();
        list->head_ = p;
        list->pagesize_++;
        list->tail_ = p;
        list->latest_ = p;

    }
    else if(list->pagesize_ < bm->numPages) // pool isn't full.
    {
        // printf("pool isn't full. \n");
        list->tail_->next = (BM_PageFrame*)malloc(sizeof(BM_PageFrame));
        list->tail_->next->next = NULL;
        list->tail_->next->fix = 0;
        list->tail_->next->fix++;
        list->tail_->next->dirty = false;
        list->tail_->next->pagenum = pageNum;
        list->tail_->next->time_usec = getUsecTime();
        memcpy(list->tail_->next->data, page_handle, PAGE_SIZE);
        page->data = list->tail_->next->data;
        page->pageNum = pageNum;

        list->pagesize_++;
        list->latest_ = list->tail_->next;
        list->tail_ = list->tail_->next;
        // list->latest_ = list->tail_->next;
    }
    else // pool is full.
    {
        // If it is full, find the one that has not been used for the longest time and replace it
        // printf("pool is full. \n");
        long long mintime = list->head_->time_usec;
        BM_PageFrame* min_time_page = list->head_;
        BM_PageFrame* p = list->head_;
        while(p != NULL)
        {
            if(p->time_usec < mintime)
            {
                min_time_page = p;
                mintime = p->time_usec;
                // printf("page=[%d], mintime=[%llu].\n", p->pagenum, p->time_usec);
            }
            p = p->next;
        }

        // Replace with new page
        p = list->head_;

        while(p != NULL)
        {
            if(min_time_page == p)
            {
                // printf("is full, head is the node, delete head.\n");
                if(p->dirty == true)
                {
                    BM_PageHandle bp;
                    bp.data = p->data;
                    bp.pageNum = p->pagenum;
                    forcePage(bm, &bp);
                }

                memcpy( p->data, page_handle, PAGE_SIZE);
                p->dirty = false;
                p->fix++;
                p->pagenum = pageNum;
                p->time_usec = getUsecTime();
                page->data = p->data;
                page->pageNum = pageNum;

                list->latest_ = p->next;
            }
            p = p->next;
        }
    }    

    free(page_handle);

    return RC_OK;
}

RC fifo(BM_BufferPool *const bm, BM_PageHandle *const page, 
		const PageNumber pageNum)
{
    BM_PageList* list = NULL;
    list = bm->mgmtData;
    if(bm->mgmtData == NULL)
    {
        bm->mgmtData = malloc(sizeof(BM_PageList));
        list = bm->mgmtData;
        list->head_ = NULL;
        list->latest_ = NULL;
        list->pagesize_ = 0;
        list->tail_ = NULL;
        list->num_read_io_ = 0;
        list->num_write_io_ = 0;
    }

    BM_PageFrame *p = list->head_;
    while(p != NULL)
    {
        if(p->pagenum == pageNum)
        {
            page->pageNum = p->pagenum;
            p->fix++;
            // memcpy(page->data, p->data, PAGE_SIZE);
            return RC_OK;
        }
        p = p->next;
    }

    SM_FileHandle file_handle;
    int ret = openPageFile(bm->pageFile, &file_handle);
    if (RC_OK != ret)
        return ret;
    
    
    if(file_handle.totalNumPages == pageNum)
    {
        SM_PageHandle mem = (char*)malloc(PAGE_SIZE);
        memset(mem, '\0', PAGE_SIZE);
        writeBlock(pageNum, &file_handle, mem);
        free(mem);
    }

    SM_PageHandle page_handle = (char*)malloc(PAGE_SIZE);
    ret = readBlock(pageNum, &file_handle, page_handle);
    ((BM_PageList*)(bm->mgmtData))->num_read_io_++;
    if(RC_OK != ret)
        return ret;
    closePageFile(&file_handle); 
    // p = bm->mgmtData;
    p = list->head_;
    if(p == NULL)
    {
        // printf("pool isn't full. \n");
        p = (void *)malloc(sizeof(BM_PageFrame));
        // p = bm->mgmtData;
        p->next = NULL;
        p->pagenum = pageNum;
        memcpy( p->data, page_handle, PAGE_SIZE);
        p->fix = 0;
        p->fix++;
        p->dirty = false;
        page->data = p->data;
        page->pageNum = p->pagenum;

        list->head_ = p;
        list->pagesize_++;
        list->tail_ = p;
        list->latest_ = p;

    }
    else
    {
        if(list->pagesize_ < bm->numPages)
        {
            // printf("pool isn't full. \n");
            list->tail_->next = (BM_PageFrame*)malloc(sizeof(BM_PageFrame));
            list->tail_->next->next = NULL;
            list->tail_->next->fix = 0;
            list->tail_->next->fix++;
            list->tail_->next->dirty = false;
            list->tail_->next->pagenum = pageNum;
            memcpy(list->tail_->next->data, page_handle, PAGE_SIZE);
            page->data = list->tail_->next->data;
            page->pageNum = pageNum;

            list->pagesize_++;
            list->latest_ = list->tail_->next;
            list->tail_ = list->tail_->next;
            // list->latest_ = list->tail_->next;
        }
        else 
        {
            // printf("pool is full. \n");
            BM_PageFrame *q = list->latest_;
            while(q->next != NULL)
            {
                // printf("is full, search from lastest.\n");
                if(q->next->fix == 0)
                {
                    if(q->next->dirty == true)
                    {
                        BM_PageHandle bp;
                        bp.data = q->next->data;
                        bp.pageNum = q->next->pagenum;
                        forcePage(bm, &bp);
                    }

                    memcpy( q->next->data, page_handle, PAGE_SIZE);
                    q->next->dirty = false;
                    q->next->fix++;
                    q->next->pagenum = pageNum;

                    page->data = q->next->data;
                    page->pageNum = pageNum;

                    list->latest_ = q->next;

                    break;
                }
                q = q->next;
            }

            if(q->next == NULL)
            {
                // printf("is full, continue search from head to replase.\n");

                q = list->head_;
                while(q != NULL)
                {
                    if(q->fix == 0)
                    {
                        if(q->dirty == true)
                        {
                            BM_PageHandle bp;
                            bp.data = q->data;
                            bp.pageNum = q->pagenum;
                            forcePage(bm, &bp);
                        }
                        
                        memcpy( q->data, page_handle, PAGE_SIZE);
                        q->dirty = false;
                        q->fix++;
                        q->pagenum = pageNum;

                        page->data = q->data;
                        page->pageNum = pageNum;

                        list->latest_ = q;
                        break;
                    }
                    q = q->next;
                }
            }

            if(q == NULL)
            {
                // printf("no answer, maybe need to replace fix==1 data.\n");
            }


        }
    }    

    free(page_handle);

    return RC_OK;
}

PageNumber *getFrameContents (BM_BufferPool *const bm)
{
    int * int_arr = malloc(bm->numPages * sizeof(int));
    BM_PageList* list = (BM_PageList*)(bm->mgmtData);
    BM_PageFrame* p = list->head_;
    for(int i = 0; i < bm->numPages; i++)
    {
        if(p != NULL)
        {
            int_arr[i] = p->pagenum;
            p = p->next;
        }
        else
        {
            int_arr[i] = NO_PAGE;
        }
    }

    return int_arr;
}

bool *getDirtyFlags (BM_BufferPool *const bm)
{
    bool * bool_arr = malloc(bm->numPages * sizeof(bool));
    BM_PageList* list = (BM_PageList*)(bm->mgmtData);
    BM_PageFrame* p = list->head_;
    for(int i = 0; i < bm->numPages; i++)
    {
        if(p != NULL)
        {
            bool_arr[i] = p->dirty;
            p = p->next;
        }
        else
        {
            bool_arr[i] = false;
        }
    }

    return bool_arr;
 }

int *getFixCounts (BM_BufferPool *const bm)
{
    int * int_arr = malloc(bm->numPages * sizeof(int));
    BM_PageList* list = (BM_PageList*)(bm->mgmtData);
    BM_PageFrame* p = list->head_;
    for(int i = 0; i < bm->numPages; i++)
    {
        if(p != NULL)
        {
            int_arr[i] = p->fix;
            p = p->next;
        }
        else
        {
            int_arr[i] = 0;
        }
    }

    return int_arr;
}

long long *getTimeUsecs (BM_BufferPool *const bm)
{
    long long * int_arr = malloc(bm->numPages * sizeof(long long));
    BM_PageList* list = (BM_PageList*)(bm->mgmtData);
    BM_PageFrame* p = list->head_;
    for(int i = 0; i < bm->numPages; i++)
    {
        if(p != NULL)
        {
            int_arr[i] = p->time_usec;
            p = p->next;
        }
        else
        {
            int_arr[i] = 0;
        }
    }

    return int_arr;
}

int getNumReadIO (BM_BufferPool *const bm)
{
    return ((BM_PageList*)(bm->mgmtData))->num_read_io_;
}

int getNumWriteIO (BM_BufferPool *const bm)
{
    return ((BM_PageList*)(bm->mgmtData))->num_write_io_;
}
