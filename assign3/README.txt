===0. project dir tree
assign3
--- README.txt
--- Makefile
--- buffer_mgr.h
--- buffer_mgr.c
--- buffer_mgr_stat.h
--- buffer_mgr_stat.c
--- dberror.h
--- dberror.c
--- expr.h
--- expr.c
--- dt.h
--- tables.h
--- storage_mgr.h
--- storage_mgr.c
--- record_mgr.h
--- record_mgr.c
--- rm_serializer.h
--- rm_serializer.c 
--- test_assign3_1.c
--- test_expr.c
--- test_helper.h

Cooperated with: 
Lei Chen    A20490798 isolatedream@gmail.com
Xingran GUO A20479094 1516539616@qq.com
Lang PAN    A20474847 panlang@mail.ustc.edu.cn


===1. compiler & run test_case
make
./test_assign3

make test_expr 
./test_expr


===2. describes solution

// Bookkeeping for scans
typedef struct RM_ScanHandle
{
	RM_TableData *rel;
	void *mgmtData;
} RM_ScanHandle;

// Record operation object of the table
typedef struct RM_TableRecord
{
	BM_PageHandle pageHandle; 
	BM_BufferPool bufferPool;	
	RID record_id;
	Expr *condition;  // scan condition
	int total_cnt; // table total records
	int cur_free_page; 
	int cur_scan_cnt;
} RM_TableRecord;

// table and manager

// only print some information.
extern RC initRecordManager (void *mgmtData);

// free RM_TableRecord mem.
extern RC shutdownRecordManager ();

// createTable, Save the schema information to the file.
extern RC createTable (char *name, Schema *schema);

// openTable, Load file table into memory, shcema of page0
extern RC openTable (RM_TableData *rel, char *name);

// closeTable, Memory to file
extern RC closeTable (RM_TableData *rel);

// deleteTable.
extern RC deleteTable (char *name);

// Get all records of the table.
extern int getNumTuples (RM_TableData *rel);

// handling records in a table
// Find a slot of the free page and insert; 
// The first character is +
extern RC insertRecord (RM_TableData *rel, Record *record);

// Find the record according to the id, and assign the first character to -
extern RC deleteRecord (RM_TableData *rel, RID id);

// Find records based on id and update
extern RC updateRecord (RM_TableData *rel, Record *record);

// Find records based on id
extern RC getRecord (RM_TableData *rel, RID id, Record *record);

// scans
// Initialize scan
extern RC startScan (RM_TableData *rel, RM_ScanHandle *scan, Expr *cond);

// Traverse search according to condition
extern RC next (RM_ScanHandle *scan, Record *record);

// closeScan
extern RC closeScan (RM_ScanHandle *scan);

// dealing with schemas
// return record size.
extern int getRecordSize (Schema *schema);

// createSchema
extern Schema *createSchema (int numAttr, char **attrNames, DataType *dataTypes, int *typeLength, int keySize, int *keys);

// freeSchema
extern RC freeSchema (Schema *schema);

// dealing with records and attribute values
// createRecord a record
extern RC createRecord (Record **record, Schema *schema);
// freeRecord a record
extern RC freeRecord (Record *record);
// get a attr form a record.
extern RC getAttr (Record *record, Schema *schema, int attrNum, Value **value);
// set a attr form a record.
extern RC setAttr (Record *record, Schema *schema, int attrNum, Value *value);
// get a slot from a free page.
extern int getPageNextSlot(char *data, int rsize);
#endif // RECORD_MGR_H

