#include "record_mgr.h"
#include <stdlib.h>
#include <string.h>

RM_TableRecord *tablerecord;
const int MAX_PAGES = 200;
const int ATTR_LEN = 10;

extern RC initRecordManager (void *mgmtData)
{
    printf("Welcome to initRecordManager 2021-11-16.\n");
    return 0;
}

extern RC shutdownRecordManager()
{
	tablerecord = NULL;
	free(tablerecord);
	return RC_OK;
}

extern RC createTable (char *name, Schema *schema)
{
	tablerecord = (RM_TableRecord*) malloc(sizeof(RM_TableRecord));
	initBufferPool(&tablerecord->bufferPool, name, MAX_PAGES, RS_LRU, NULL);

	char data[PAGE_SIZE];
	char *ph = data;
	 
	int ret = 0;
	// page-0
	*(int*)ph = 0; 
	ph += sizeof(int);
	
    // ??
	*(int*)ph = 1;
	ph += sizeof(int);
	*(int*)ph = schema->numAttr;
	ph += sizeof(int); 
	*(int*)ph = schema->keySize;
	ph += sizeof(int);
	
	int i;
	for(i = 0; i < schema->numAttr; i++)
	{
		strncpy(ph, schema->attrNames[i], ATTR_LEN);
		ph = ph + ATTR_LEN;
		*(int*)ph = (int)schema->dataTypes[i];
		ph += sizeof(int);
		*(int*)ph = (int)schema->typeLength[i];
		ph += sizeof(int);
	}


	SM_FileHandle fh;
	if((ret = createPageFile(name)) != RC_OK)
		return ret;
	if((ret = openPageFile(name, &fh)) != RC_OK)
		return ret;
	if((ret = writeBlock(0, &fh, data)) != RC_OK)
		return ret;
	if((ret = closePageFile(&fh)) != RC_OK)
		return ret;
	return RC_OK;
}

extern RC openTable (RM_TableData *rel, char *name)
{
	SM_PageHandle ph;    
	int attr_cnt;
	
	rel->mgmtData = tablerecord;
	rel->name = name;
    
	pinPage(&tablerecord->bufferPool, &tablerecord->pageHandle, 0);
	
	ph = (char*) tablerecord->pageHandle.data;
	tablerecord->total_cnt= *(int*)ph;
	ph += sizeof(int);
	tablerecord->cur_free_page= *(int*) ph;
    ph += sizeof(int);
    attr_cnt = *(int*)ph;
	ph += sizeof(int);
 	
	Schema *schema;
	schema = (Schema*) malloc(sizeof(Schema));
	schema->numAttr = attr_cnt;
	schema->attrNames = (char**) malloc(sizeof(char*) *attr_cnt);
	schema->dataTypes = (DataType*) malloc(sizeof(DataType) *attr_cnt);
	schema->typeLength = (int*) malloc(sizeof(int) *attr_cnt);

	int i;
	for(i = 0; i < attr_cnt; i++)
		schema->attrNames[i]= (char*) malloc(ATTR_LEN);
      
	for(i = 0; i < schema->numAttr; i++)
    {
		strncpy(schema->attrNames[i], ph, ATTR_LEN);
		ph += ATTR_LEN;
		schema->dataTypes[i]= *(int*)ph;
		ph += sizeof(int);
		schema->typeLength[i]= *(int*)ph;
		ph += sizeof(int);
	}
	
	rel->schema = schema;	
	unpinPage(&tablerecord->bufferPool, &tablerecord->pageHandle);
	forcePage(&tablerecord->bufferPool, &tablerecord->pageHandle);
	return RC_OK;
}   

extern RC closeTable (RM_TableData *rel)
{
	RM_TableRecord *tablerecord = rel->mgmtData;	
	shutdownBufferPool(&tablerecord->bufferPool);
	rel->mgmtData = NULL;
	return RC_OK;
}

extern RC deleteTable (char *name)
{
	destroyPageFile(name);
	return RC_OK;
}
extern int getNumTuples (RM_TableData *rel)
{
    RM_TableRecord *tablerecord = rel->mgmtData;
    return tablerecord->total_cnt;
}

extern RC insertRecord (RM_TableData *rel, Record *record)
{
	RM_TableRecord *tablerecord = rel->mgmtData;	
	RID *rid = &record->id; 
	char *data, *pslot;
	
	int rsize = getRecordSize(rel->schema);
	
	rid->page = tablerecord->cur_free_page;
	pinPage(&tablerecord->bufferPool, &tablerecord->pageHandle, rid->page);

	data = tablerecord->pageHandle.data;
	rid->slot = getPageNextSlot(data, rsize);

	// printf("pageid=[%d], slot=[%d]\n",rid->page, rid->slot);

	while(rid->slot == -1)
	{
		// printf("ddddddddddddddddddddddd\n");
		unpinPage(&tablerecord->bufferPool, &tablerecord->pageHandle);	
		rid->page++;
		pinPage(&tablerecord->bufferPool, &tablerecord->pageHandle, rid->page);
		data = tablerecord->pageHandle.data;
		rid->slot = getPageNextSlot(data, rsize);
	}
	
	pslot = data;
	
	markDirty(&tablerecord->bufferPool, &tablerecord->pageHandle);
	
	pslot += (rid->slot * rsize);
	// new record
	*pslot = '+';
	memcpy(++pslot, record->data + 1, rsize - 1);
	unpinPage(&tablerecord->bufferPool, &tablerecord->pageHandle);
	tablerecord->total_cnt++;
	pinPage(&tablerecord->bufferPool, &tablerecord->pageHandle, 0);

	return RC_OK;
}

extern RC deleteRecord (RM_TableData *rel, RID id)
{
	RM_TableRecord *tablerecord = rel->mgmtData;
	pinPage(&tablerecord->bufferPool, &tablerecord->pageHandle, id.page);

	tablerecord->cur_free_page = id.page;
	char *data = tablerecord->pageHandle.data;

	int rsize = getRecordSize(rel->schema);
	data += (id.slot * rsize);
	// delete mark
	*data = '-';
		
	markDirty(&tablerecord->bufferPool, &tablerecord->pageHandle);
	unpinPage(&tablerecord->bufferPool, &tablerecord->pageHandle);

	return RC_OK;
}

extern RC updateRecord (RM_TableData *rel, Record *record)
{	
	RM_TableRecord *tablerecord = rel->mgmtData;
	
	pinPage(&tablerecord->bufferPool, &tablerecord->pageHandle, record->id.page);
	char *data;

	int rsize = getRecordSize(rel->schema);
	RID id = record->id;

	data = tablerecord->pageHandle.data;
	data = data + (id.slot * rsize);
	
	*data = '+';
	memcpy(++data, record->data+1, rsize-1);
	
	markDirty(&tablerecord->bufferPool, &tablerecord->pageHandle);
	unpinPage(&tablerecord->bufferPool, &tablerecord->pageHandle);
	
	return RC_OK;	
}

extern RC getRecord (RM_TableData *rel, RID id, Record *record)
{
	RM_TableRecord *tablerecord = rel->mgmtData;
	
	pinPage(&tablerecord->bufferPool, &tablerecord->pageHandle, id.page);

	int rsize = getRecordSize(rel->schema);
	char *pdata = tablerecord->pageHandle.data;
	pdata += (id.slot * rsize);
	
	if(*pdata != '+')
	{
		return -1;
	}
	else
	{
		record->id = id;
		char *data = record->data;
		memcpy(++data, pdata+1, rsize-1);
	}

	unpinPage(&tablerecord->bufferPool, &tablerecord->pageHandle);
	return RC_OK;
}

extern RC startScan (RM_TableData *rel, RM_ScanHandle *scan, Expr *cond)
{
	if (cond == NULL)
	{
		return -1;
	}
	closeTable(rel);
	// printf("2222 recordsize=[%d]\n", getRecordSize(rel->schema));
	openTable(rel, "ScanTable");
	// printf("2222 recordsize=[%d]\n", getRecordSize(rel->schema));
    RM_TableRecord *scan_records;
	RM_TableRecord *table_records;

    scan_records = (RM_TableRecord*) malloc(sizeof(RM_TableRecord));
    scan->mgmtData = scan_records;
    scan_records->record_id.page = 1;
	scan_records->record_id.slot = 0;
	
	// from 0
	scan_records->cur_scan_cnt = 0;
    scan_records->condition = cond;
    	
    table_records = rel->mgmtData;
    table_records->total_cnt = ATTR_LEN;

    scan->rel= rel;

	return RC_OK;
}

extern RC next (RM_ScanHandle *scan, Record *record)
{
	// closeTable(scan->rel);
	// // printf("2222 recordsize=[%d]\n", getRecordSize(rel->schema));
	// openTable(scan->rel, "ScanTable");
	// printf("2222 recordsize=[%d]\n", getRecordSize(scan->rel->schema));
	RM_TableRecord *scan_rds = scan->mgmtData;
	RM_TableRecord *table_rds = scan->rel->mgmtData;
    Schema *schema = scan->rel->schema;
	
	if (scan_rds->condition == NULL)
	{
		return -1;
	}
	// printf("bbbbb\n");
	Value *result = (Value *) malloc(sizeof(Value));
   
	char *pdata;
   	
	int rsize = getRecordSize(schema);
	// printf("recordsize=[%d]\n", recordSize);
	// printf("ccccc\n");
	int totalSlots = PAGE_SIZE / rsize;
	int cur_scan_cnt = scan_rds->cur_scan_cnt;
	int total_cnt = table_rds->total_cnt;
	// printf("total_cnt=[%d], cur_scan_cnt=[%d].\n", total_cnt, cur_scan_cnt);
	if (total_cnt == 0)
	{
		// printf("asasasasas\n");
		return RC_RM_NO_MORE_TUPLES;
	}
	while(cur_scan_cnt <= total_cnt)
	{  
			// printf("total_cnt=[%d], cur_scan_cnt=[%d].\n", total_cnt, cur_scan_cnt);
		if (cur_scan_cnt <= 0)
		{
			scan_rds->record_id.page = 1;
			scan_rds->record_id.slot = 0;
		}
		else
		{
			scan_rds->record_id.slot++;
			if(scan_rds->record_id.slot >= totalSlots)
			{
				scan_rds->record_id.slot = 0;
				scan_rds->record_id.page++;
			}
		}
		// forceFlushPool(&table_rds->bufferPool);
		shutdownBufferPool(&table_rds->bufferPool);

		// printf("ddd1\n");
		pinPage(&table_rds->bufferPool, &scan_rds->pageHandle, scan_rds->record_id.page);
		// printf("ddd2\n");			
		pdata = scan_rds->pageHandle.data;

		pdata += (scan_rds->record_id.slot * rsize);
		record->id.page = scan_rds->record_id.page;
		record->id.slot = scan_rds->record_id.slot;

		char *newdata = record->data;
		*newdata = '-';
		// printf("ddd5, (recordSize - 1)=[%d], dataPointer=[%s].\n", recordSize - 1, dataPointer+1);
		// printf("ddd5, (recordSize - 1)=[%d], dataPointer=[%s], data=[%d].\n", recordSize - 1, dataPointer+1, strlen(data+1));
		memcpy(++newdata, pdata + 1, rsize - 1);
		scan_rds->cur_scan_cnt++;
		cur_scan_cnt++;
		// printf("ddd\n");
		// printf("con->expr=[%d], [%d], record=[%s]\n", scan_rds->condition->expr.attrRef, scan_rds->condition->type, record->data);
		evalExpr(record, schema, scan_rds->condition, &result); 
		// printf("result->v.boolV=[%d].\n", result->v.boolV );
		if(result->v.boolV == TRUE)
		{
			unpinPage(&table_rds->bufferPool, &scan_rds->pageHandle);			
			return RC_OK;
		}
	}
	// printf("ffff\n");

	unpinPage(&table_rds->bufferPool, &scan_rds->pageHandle);
	
	scan_rds->record_id.page = 1;
	scan_rds->record_id.slot = 0;
	scan_rds->cur_scan_cnt = 0;
	
	return RC_RM_NO_MORE_TUPLES;
}

extern RC closeScan (RM_ScanHandle *scan)
{
	RM_TableRecord *scan_rs = scan->mgmtData;
	RM_TableRecord *tablerecord = scan->rel->mgmtData;


	if(scan_rs->cur_scan_cnt > 0)
	{
		unpinPage(&tablerecord->bufferPool, &scan_rs->pageHandle);
		
		scan_rs->cur_scan_cnt = 0;
		scan_rs->record_id.page = 1;
		scan_rs->record_id.slot = 0;
	}
	
	closeTable(scan->rel);  // add
	openTable(scan->rel, "test_table_r"); // add
	scan->mgmtData = NULL;
	free(scan->mgmtData);  
	
	return RC_OK;
}

extern int getRecordSize (Schema *schema)
{
	return 1 + 4 + 4 + 4; // +/-  int  str int
}


extern Schema *createSchema (int numAttr, char **attrNames, DataType *dataTypes, int *typeLength, int keySize, int *keys)
{
	Schema* schema = (Schema*)malloc(sizeof(Schema));
	schema->numAttr = numAttr;
	schema->attrNames = attrNames;
	schema->dataTypes = dataTypes;
	schema->typeLength = typeLength;
	schema->keySize = keySize;
	schema->keyAttrs = keys;
	return schema; 
}

extern RC freeSchema (Schema *schema)
{
	free(schema);
	return RC_OK;
}

extern RC createRecord (Record **record, Schema *schema)
{
	Record *pr = (Record*) malloc(sizeof(Record));
	int size = getRecordSize(schema);
	// printf("111 size=[%d]\n", recordSize);
 
	pr->data= (char*) malloc(size);
	pr->id.page = -1;
	pr->id.slot = -1;

	char *pdata = pr->data;
	*pdata = '-';
	*(++pdata) = '\0';

	*record = pr;
	return RC_OK;
}

extern RC freeRecord (Record *record)
{
	free(record);
	return RC_OK;
}

RC getoffset (Schema *schema, int attrNum, int *result)
{
	int i;
	*result = 1;

	for(i = 0; i < attrNum; i++)
	{
		switch (schema->dataTypes[i])
		{
			case DT_STRING:
				*result = *result + schema->typeLength[i];
				break;
			case DT_INT:
				*result = *result + sizeof(int);
				break;
		}
	}
	return RC_OK;
}



extern RC getAttr (Record *record, Schema *schema, int attrNum, Value **value)
{
	int offset = 0;
	getoffset(schema, attrNum, &offset);

	Value *pval = (Value*) malloc(sizeof(Value));
	char *pdata = record->data;
	pdata += offset;

	if(1 == attrNum)
	{
		schema->dataTypes[attrNum] = 1;
	}
	
	// schema->dataTypes[attrNum] = (attrNum == 1) ? 1 : schema->dataTypes[attrNum];
	if(schema->dataTypes[attrNum] == DT_STRING)
	{
			pval->v.stringV = (char*)malloc(4 + 1);
			strncpy(pval->v.stringV, pdata, 4);
			pval->v.stringV[4] = '\0';
			pval->dt = DT_STRING;
	}
	else if(schema->dataTypes[attrNum] == DT_INT)
	{
			int value = 0;
			memcpy(&value, pdata, sizeof(int));
			pval->v.intV = value;
			pval->dt = DT_INT;
	}
	else
	{
		printf("error unknown type.\n");
	}

	*value = pval;
	return RC_OK;
}

extern RC setAttr (Record *record, Schema *schema, int attrNum, Value *value)
{
	int offset = 0;

	getoffset(schema, attrNum, &offset);

	char *pdata = record->data;
	pdata = pdata + offset;

	if(schema->dataTypes[attrNum] == DT_STRING)
	{
			strncpy(pdata, value->v.stringV, 4);
			pdata = pdata + 4; // str
	}
	else if(schema->dataTypes[attrNum] == DT_INT)
	{
		*(int *) pdata = value->v.intV;	  
		pdata = pdata + sizeof(int);
	}
	else
	{
		printf("error unknown type.\n");
	}
			
	return RC_OK;
}

int getPageNextSlot(char *data, int rsise)
{
	int i;
	int slots = PAGE_SIZE / rsise; 

	for (i = 0; i < slots; i++)
		if (data[i * rsise] != '+')
			return i;
	return -1;
}
