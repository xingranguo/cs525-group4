===0. project dir tree
assign1
--- dberror.c
--- dberror.h
--- Makefile
--- README.txt
--- storage_mgr.c
--- storage_mgr.h
--- test_assign1_1.c
--- test_helper.h

Cooperated with: 
Lei Chen    A20490798 isolatedream@gmail.com
Xingran GUO A20479094 1516539616@qq.com
Lang PAN    A20474847 panlang@mail.ustc.edu.cn


===1. compiler & run test_case
make
./test_assign1

===2. memory check 
valgrind --leak-check=full --show-reachable=yes --trace-children=yes ./test_assign1

===3. describes solution
// use void *mgmtInfo  Store FILE pointer.
typedef struct SM_FileHandle {
	char *fileName;
	int totalNumPages;
	int curPagePos;
	void *mgmtInfo;
} SM_FileHandle;



/* manipulating page files */



// only printf some information.
extern void initStorageManager (void);

// Create a new page file fileName. The initial file size is one page(4096). 
// This method should this single page with '\0' bytes.
extern RC createPageFile (char *fileName);

// Opens an existing page file. return RC_FILE_NOT_FOUND if the file does not exist.
// initialize fHandle struct, use void *mgmtInfo  Store FILE pointer.
extern RC openPageFile (char *fileName, SM_FileHandle *fHandle);

// Close an open page file, through file pointer.
extern RC closePageFile (SM_FileHandle *fHandle);

// call remove() remove the file.
extern RC destroyPageFile (char *fileName);



/* reading blocks from disc */

// The method reads the block at position pageNum from a file and stores its content in the memory pointed
// to by the memPage page handle.
// if the file has error input pageNum pages, the method will return RC_READ_NON_EXISTING_PAGE
// through fseek Move to the right page position, then fread the datas, store in memPage.
extern RC readBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage);


// Return the current page position in a file
extern int getBlockPos (SM_FileHandle *fHandle);

// call readBlock to read first block. 
extern RC readFirstBlock (SM_FileHandle *fHandle, SM_PageHandle memPage);

// call readBlock to read previous block. relative to the curPagePos
extern RC readPreviousBlock (SM_FileHandle *fHandle, SM_PageHandle memPage);

// call readBlock to read current block. relative to the curPagePos
extern RC readCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage);

// call readBlock to read next block. relative to the curPagePos
extern RC readNextBlock (SM_FileHandle *fHandle, SM_PageHandle memPage);

// call readBlock to read last block. relative to the totalNumPages
extern RC readLastBlock (SM_FileHandle *fHandle, SM_PageHandle memPage);



/* writing blocks to a page file */

// Write a page to disk using either the current position or an absolute position.
// if the pageNum is error, return RC_FILE_NOT_FOUND.
// if the first initial page, call appendEmptyBlock()
// through fseek() move to the right position
// through fwrite() write the page to disk
// if add a new page, call ensureCapacity()
extern RC writeBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage);

// call writeBlock
extern RC writeCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage);

// Increase the number of pages in the file by one. And filled with zero bytes.
extern RC appendEmptyBlock (SM_FileHandle *fHandle);

// If the file has less than numberOfPages pages then increase the size to numberOfPages.
extern RC ensureCapacity (int numberOfPages, SM_FileHandle *fHandle);
