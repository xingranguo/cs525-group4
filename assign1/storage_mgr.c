#include "storage_mgr.h"
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>

// typedef struct FileMemExt
// {
//    FILE *file_pointer_;
//    int file_no_; 
// }FileMemExt;

extern void initStorageManager (void)
{
    printf("Welcome to our iit's db-system StorageManager 2021-12-12.\n");
}

// Create a new page file fileName. The initial file size should be one page. This method should fill this
// single page with ’\0’ bytes.
extern RC createPageFile (char *fileName)
{
    if (fileName == NULL)
        return RC_FILE_HANDLE_NOT_INIT; 
    
    FILE* f1 = fopen(fileName, "w");   // write or create
    if  (NULL == f1)
    {
        perror("fopen");
        return RC_FILE_HANDLE_NOT_INIT;
    }

    // need to write 4096 byte(\0) for first page
    char initPage[PAGE_SIZE];
    memset(initPage, '\0', sizeof(initPage));
    
    if(1 != fwrite(initPage, PAGE_SIZE, 1, f1))
    {
        perror("fwirte");
        return RC_WRITE_FAILED;
    }

    fclose(f1);

    return RC_OK;
}

// • openPageFile
// – Opens an existing page file. Should return RC FILE NOT FOUND if the file does not exist.
// – The second parameter is an existing file handle.
// – If opening the file is successful, then the fields of this file handle should be initialized with the information
// about the opened file. For instance, you would have to read the total number of pages that are stored
// in the file from disk.
extern RC openPageFile (char *fileName, SM_FileHandle *fHandle)
{
    if (fileName == NULL || fHandle == NULL)
        return RC_FILE_NOT_FOUND;
    
    
    FILE* f1 = fopen(fileName, "r+"); // r+ read and write, not create
    if( NULL == f1)
    {
        perror("fopen");
        return RC_FILE_NOT_FOUND;
    }

    struct stat st;
    fstat(f1->_fileno, &st);

    fHandle->fileName = fileName;
    fHandle->totalNumPages = st.st_size / PAGE_SIZE;
    // fHandle->totalNumPages = st.st_size / PAGE_SIZE + st.st_size % PAGE_SIZE;
    fHandle->curPagePos = 0;
    // fHandle->mgmtInfo = malloc(sizeof(FileMemExt));
    // ((FileMemExt *)fHandle->mgmtInfo)->file_no_ = f1->_fileno;
    // ((FileMemExt *)fHandle->mgmtInfo)->file_pointer_ = f1;
    fHandle->mgmtInfo = f1;

    // printf("file size is [%lld], page size=[%d].\n", st.st_size, fHandle->totalNumPages);

    // fclose(f1);

    return RC_OK;
}

// – Close an open page file 
extern RC closePageFile (SM_FileHandle *fHandle)
{
    if ( NULL == fHandle)
        return RC_FILE_HANDLE_NOT_INIT;
    
    // if(0 != fclose(fdopen(((FileMemExt*)fHandle->mgmtInfo)->fileno_, "r+") ))
    // if(0 != fclose(((FileMemExt*)fHandle->mgmtInfo)->file_pointer_ ))
    if(0 != fclose(fHandle->mgmtInfo))
    {
        // free(fHandle->mgmtInfo);
        fHandle->totalNumPages = -1;
        perror("fclose");
        return RC_FILE_NOT_FOUND;
    }
    fHandle->totalNumPages = -1;
    // free(fHandle->mgmtInfo);
    return RC_OK;
}

// - destroy (delete) a page file.
extern RC destroyPageFile (char *fileName)
{
    if (0 != remove(fileName))
    {
        perror("remove");
        return RC_FILE_NOT_FOUND;
    }

    return RC_OK;
}

/* reading blocks from disc */

// • readBlock
// – The method reads the block at position pageNum from a file and stores its content in the memory pointed
// to by the memPage page handle.
// – If the file has less than pageNum pages, the method should return RC READ NON EXISTING PAGE. 
// • getBlockPos
// – Return the current page position in a file
extern RC readBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    if(pageNum < 0 || pageNum >= fHandle->totalNumPages)
        return RC_READ_NON_EXISTING_PAGE;

    // if(0 != fseek( fdopen(((FileMemExt*)fHandle->mgmtInfo)->fileno_, "r+"), PAGE_SIZE * pageNum, SEEK_SET))
    // if(0 != fseek( ((FileMemExt*)fHandle->mgmtInfo)->file_pointer_, PAGE_SIZE * pageNum, SEEK_SET))
    if(0 != fseek(fHandle->mgmtInfo, PAGE_SIZE * pageNum, SEEK_SET))
    {
        perror("fseek");
        return RC_READ_NON_EXISTING_PAGE;
    }

    // if(1 != fread(memPage, PAGE_SIZE, 1, fdopen(((FileMemExt*)fHandle->mgmtInfo)->fileno_, "r+") ))
    // if(1 != fread(memPage, PAGE_SIZE, 1, ((FileMemExt*)fHandle->mgmtInfo)->file_pointer_ ))
    if(1 != fread(memPage, PAGE_SIZE, 1, fHandle->mgmtInfo ))
    {
        perror("fread");
        return RC_READ_NON_EXISTING_PAGE;
    }

    fHandle->curPagePos = pageNum;   

    return RC_OK;
}

extern int getBlockPos (SM_FileHandle *fHandle)
{
    return fHandle->curPagePos;
}

// • readFirstBlock , readLastBlock
// – Read the first respective last page in a file
extern RC readFirstBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    return readBlock(0, fHandle, memPage);
}

extern RC readLastBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    return readBlock(fHandle->totalNumPages-1, fHandle, memPage);
}

// • readPreviousBlock , readCurrentBlock , readNextBlock
// – Read the current, previous, or next page relative to the curPagePos of the file.
// – The curPagePos should be moved to the page that was read.
// – If the user tries to read a block before the first page or after the last page of the file, the method should
// return RC READ NON EXISTING PAGE.

extern RC readPreviousBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    return readBlock(fHandle->curPagePos-1, fHandle, memPage);
}

extern RC readCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    return readBlock(fHandle->curPagePos, fHandle, memPage);
}

extern RC readNextBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    return readBlock(fHandle->curPagePos+1, fHandle, memPage);
}



/* writing blocks to a page file */
//  • writeBlock , writeCurrentBlock
// – Write a page to disk using either the current position or an absolute position.
extern RC writeBlock (int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    // 1. check pagenum
    if (pageNum < 0 || pageNum > fHandle->totalNumPages) // if pageNum == Pages, new page
        return RC_FILE_NOT_FOUND;

    // 2. get already opened file's pointer
    // FILE* p_file = fdopen(((FileMemExt*)fHandle->mgmtInfo)->fileno_, "r+");
    // FILE* p_file = ((FileMemExt*)fHandle->mgmtInfo)->file_pointer_;
    FILE* p_file = fHandle->mgmtInfo;
    // printf("p_file=[%p]\n", p_file);
    // 3. if new page, initial
    if (pageNum == fHandle->totalNumPages)
        appendEmptyBlock(fHandle);

    // // 4. get right pos
    // fpos_t pos;
    // fgetpos(p_file, &pos);
    // printf("pos3=%d\n", pos.__pos);
    fseek( p_file, pageNum * PAGE_SIZE, SEEK_SET);
    // fgetpos(p_file, &pos);
    // printf("pos4=%d\n", pos.__pos);

    // 5. update or write data
    //https://stackoverflow.com/questions/37344556/in-linux-fwrite-command-does-not-set-errno-how-to-get-proper-errno-on-failure-c
    if (1 != fwrite(memPage, PAGE_SIZE, 1, p_file))
    {
        perror("fwrite");
        return RC_WRITE_FAILED;
    }

    // 5. fflush mem to disk, need fflush or fclose 
    // 6. maybe need to update handle pagenums
    ensureCapacity(pageNum, fHandle);

    return RC_OK;
}

extern RC writeCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage)
{
    return writeBlock(fHandle->curPagePos, fHandle, memPage);
}


// • appendEmptyBlock
// – Increase the number of pages in the file by one. The new last page should be filled with zero bytes.
// • ensureCapacity
// – If the file has less than numberOfPages pages then increase the size to numberOfPages.
extern RC appendEmptyBlock (SM_FileHandle *fHandle)
{
    // FILE* f1 = fdopen(((FileMemExt*)fHandle->mgmtInfo)->fileno_, "r+");
    // FILE* f1 = ((FileMemExt*)fHandle->mgmtInfo)->file_pointer_;
    FILE* f1 = fHandle->mgmtInfo;
    if(NULL == f1)
    {
        perror("fdopen");
        return RC_FILE_NOT_FOUND;
    }
    // printf("f1=[%p]\n", f1);

    // fpos_t pos;
    // fgetpos(f1, &pos);
    // printf("pos1=%d\n", pos.__pos);

    // fseek( f1, 0, SEEK_END);

    char initPage[PAGE_SIZE];
    memset(initPage, '\0', sizeof(initPage));
    
    if(1 != fwrite(initPage, PAGE_SIZE, 1, f1))
    {
        perror("fwirte");
        return RC_WRITE_FAILED;
    }

    // fgetpos(f1, &pos);
    // printf("pos2=%d\n", pos.__pos);

    return RC_OK;
}

extern RC ensureCapacity (int numberOfPages, SM_FileHandle *fHandle)
{
    if(numberOfPages == fHandle->totalNumPages)
        ++fHandle->totalNumPages;
    return RC_OK;
}